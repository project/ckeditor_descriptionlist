CONTENTS OF THIS FILE
=====================

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Uninstallation
 * Maintainers


INTRODUCTION
============

This modules adds description list plugin into the WYSIWYG editor,
that provides the ability to create a definition list such as dl,
dt and dd html tags.

* For a full description of this module, visit the project page:
   https://www.drupal.org/project/ckeditor_descriptionlist

* To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ckeditor_descriptionlist


REQUIREMENTS
============

This module requires the following core, module and plugin:

* Drupal core ^8.8 || ^9
* CKEditor Module - core
* Description list plugin:
   https://github.com/Reinmar/ckeditor-plugin-descriptionlist


RECOMMENDED MODULES
===================

This module requires no modules outside of Drupal core.


INSTALLATION
============

1. Download the plugin from https://github.com/Reinmar/ckeditor-plugin-descriptionlist

2. Place the plugin in the root libraries folder (/libraries/ckeditor-plugin-descriptionlist).

3. Install/Enable the ckeditor_descriptionlist module as you would normally
install a contributed Drupal module.

* Visit (https://www.drupal.org/node/1897420) for further information.
* We recommend that you install the module using composer:
   composer require 'drupal/ckeditor_descriptionlist:^1.1'

4. Configure your WYSIWYG toolbar to include description list buttons.


CONFIGURATION
=============

Go to the Text formats and editors settings 'Admin -> Configuration -> Content -> Formats'
add the buttons : Tag dl, Tag dd, Tag dt to any CKEditor-enabled text format you want.


UNINSTALLATION
==============

Uninstall the module from 'Administer -> Modules'.


MAINTAINERS
===========

Current maintainers:
* Tsymi (https://www.drupal.org/u/tsymi)
