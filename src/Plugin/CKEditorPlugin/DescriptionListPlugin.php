<?php

namespace Drupal\ckeditor_descriptionlist\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "descriptionlist" plugin.
 *
 * @CKEditorPlugin(
 *   id = "descriptionlist",
 *   label = @Translation("Description List")
 * )
 */
class DescriptionListPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'DescriptionList' => [
        'label' => $this->t('Tag dl'),
        'image' => base_path() . 'libraries/ckeditor-plugin-descriptionlist/icons/descriptionlist.png',
      ],
      'DescriptionTerm' => [
        'label' => $this->t('Tag dd'),
        'image' => base_path() . 'libraries/ckeditor-plugin-descriptionlist/icons/descriptionterm.png',
      ],
      'descriptionValue' => [
        'label' => $this->t('Tag dt'),
        'image' => base_path() . 'libraries/ckeditor-plugin-descriptionlist/icons/descriptionvalue.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/ckeditor-plugin-descriptionlist/plugin.js';
  }

}
